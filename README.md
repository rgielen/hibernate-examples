Hibernate Examples
==================

This is a collection of some examples useful for Hibernate 4 courses.

The project code is released under Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0.html

