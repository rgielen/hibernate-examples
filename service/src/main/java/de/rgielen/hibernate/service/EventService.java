package de.rgielen.hibernate.service;

import de.rgielen.hibernate.model01.Event;

import javax.inject.Named;

/**
 * EventService.
 *
 * @author Rene Gielen
 */
@Named
public class EventService extends GenericEntityService<Event, Long> {

	@Override
	protected Class<Event> entityClass() {
		return Event.class;
	}
}
