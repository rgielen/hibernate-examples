package de.rgielen.hibernate.service;

import de.rgielen.hibernate.model01.Event;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring-context.xml"})
@Transactional
public class EventServiceTest {

	@Inject
	EventService eventService;

	@Test
	public void testEventServiceCanLoadAndSave() throws Exception {
		eventService.save(new Event("foo", new Date(), null));
		eventService.flush();
		final List<Event> events = eventService.findAll();
		assertTrue(events.size()>0);
		for ( Event event : events ) {
			System.out.println(event);
		}
	}

}