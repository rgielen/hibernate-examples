package de.rgielen.hibernate.model01;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Name.
 *
 * @author Rene Gielen
 */
@Embeddable
public class Name implements Serializable {

	private String firstName;
	private String lastName;

	public Name() {
	}

	public Name( String firstName, String lastName ) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName( String firstName ) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName( String lastName ) {
		this.lastName = lastName;
	}

	@Transient
	public String getName() {
		StringBuilder result = new StringBuilder();
		if (firstName != null) {
			result.append(firstName);
		}
		if (lastName != null) {
			if (result.length() > 0) {
				result.append(" ");
			}
			result.append(lastName);
		}
		return result.toString();
	}

}
