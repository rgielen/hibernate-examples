package de.rgielen.hibernate.model01;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Event.
 *
 * @author Rene Gielen
 */
@Entity
public class Event {

	private Long id;
	private String title;
	private Date startDate;
	private Person person;

	public Event() {
	}

	public Event( String title, Date startDate, Person person ) {
		this.title = title;
		this.startDate = startDate;
		this.person = person;
	}

	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="SEQ_GEN")
	@SequenceGenerator(name="SEQ_GEN", sequenceName="ev_seq", allocationSize=20)
	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle( String title ) {
		this.title = title;
	}

	@Temporal(TemporalType.DATE)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate( Date startDate ) {
		this.startDate = startDate;
	}

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
	//@JoinColumn(name="emp_no")
	//@JoinTable(name="Person_Events",
	//		   joinColumns = @JoinColumn(name="event_id"),
	//		   inverseJoinColumns = @JoinColumn(name="person_id")
	//)
	public Person getPerson() {
		return person;
	}

	public void setPerson( Person person ) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Event{" +
				"id=" + id +
				", title='" + title + '\'' +
				", startDate=" + startDate +
				", person=" + person +
				'}';
	}
}
