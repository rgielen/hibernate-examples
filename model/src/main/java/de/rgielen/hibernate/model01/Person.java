package de.rgielen.hibernate.model01;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

/**
 * Person.
 *
 * @author Rene Gielen
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name="persontype",
		discriminatorType= DiscriminatorType.STRING
)
@DiscriminatorValue("Person")
public class Person {

	private Long id;
	private Name name;
	private Long version;

	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="SEQ_GEN")
	@SequenceGenerator(name="SEQ_GEN", sequenceName="my_sequence", allocationSize=20)
	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName( Name name ) {
		this.name = name;
	}

	@Version
	public Long getVersion() {
		return version;
	}

	void setVersion( Long version ) {
		this.version = version;
	}


}
