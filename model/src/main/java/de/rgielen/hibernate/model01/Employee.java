package de.rgielen.hibernate.model01;

import javax.persistence.DiscriminatorValue;

/**
 * Employee.
 *
 * @author Rene Gielen
 */
@DiscriminatorValue("Emp")
public class Employee extends Person {

}
