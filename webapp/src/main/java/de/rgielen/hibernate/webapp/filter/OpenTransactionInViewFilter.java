package de.rgielen.hibernate.webapp.filter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * OpenTransactionInViewFilter.
 *
 * @author Rene Gielen
 */
public class OpenTransactionInViewFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(OpenTransactionInViewFilter.class);

	private PlatformTransactionManager transactionManager;

	@Override
	public void init( FilterConfig filterConfig ) throws ServletException {
		WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(
				filterConfig.getServletContext()
		);
		if (applicationContext != null) {
			transactionManager = applicationContext.getBean(PlatformTransactionManager.class);
		}
	}

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain )
			throws IOException, ServletException {

		if (transactionManager != null) {
			TransactionStatus status = createTransactionContext();

			try {
				chain.doFilter(request, response);
			} catch ( RuntimeException e ) {
				status.setRollbackOnly();
				if (LOG.isEnabledFor(Level.ERROR)) {
					LOG.error("[doFilter]: Exception catched, rolling back", e);
				}
				throw e;
			} finally {
				transactionManager.commit(status);
			}
		}

	}

	private TransactionStatus createTransactionContext() {
		TransactionStatus status;DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setName("OpenTransactionInViewFilter@" + def.hashCode());
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		// Create Transaction Context
		status = transactionManager.getTransaction(def);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[createTransactionContext]: Transaction context created.");
		}
		return status;
	}


	@Override
	public void destroy() {

	}
}
